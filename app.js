$(document).ready(function(){

    var prix_pizza = 0;
    var prix_pate = 0;
    var prix_extra = 0;
    var slices = 0;

    $(".pizza-type label").hover(
    function(){
        $(this).find(".description").show();
    },
    function(){
        $(this).find(".description").hide();
    },
    );

    

    $('.nb-parts input').on('keyup', function() {
        $(".pizza-pict").remove();
        var pizza = $('<span class="pizza-pict"></span>');
        slices = +$(this).val();

        for(i = 0; i < slices/6 ; i++) {
            $(this).after(pizza.clone().addClass('pizza-6'));
        }

        if(slices%6 != 0) $('.pizza-pict').last().removeClass('pizza-6').addClass('pizza-'+slices%6);
        prix();
    });


    $(".pizza-type input").click(
        function(){
            var count = 0;
        if("input[type='radio'][name='type']:checked"){
           count = $(this).attr("data-price");
           $(".tile").find("p").html("<p>count+€</p>");
        }
    });

    
    $(".next-step").click(
    function(){
        $(".infos-client").slideDown();
        $(".next-step").hide();
    });

    
    $(".add" ).click( function() {
        $(this).before("<br><input type='text'/>");
    });
    
    $(".done").click(function(){
        var text = $(".infos-client .type:first-child input").val();
        var prix =  $(".stick-right p").text();
       $(".typography-row").remove();
       $(".stick-right").remove();
       $(".container").append("Merci "+text+" ! Votre commande arrive dans 15 minutes ! Prix de votre commande : " + prix);   
    });

    function prix() {
        let prix = 0;

        prix += (+$("input[name='type']:checked").data("price") + $("input[name='pate']:checked").data("price")) * $(".nb-parts input").val()/6;
        $("input[name='extra']:checked").each(function(i, elem) {
            /// on récupère la valeur du prix pour chaqué élément sélectionné
            prix += $(elem).data("price");
        });
        $(".stick-right p").text(prix + " €");
    }

    $("input[data-price]").change(prix);
});

